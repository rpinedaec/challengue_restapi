# Proyecto Python

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Elaborar un proyecto que pueda responder acerca de la informacion de los jugadores de la selección peruana que asistieron al Mundial Rusia 2018. Para ello podremos consultar lo siguiente:

   - La lista de 23 jugadores de futbol.
   - La lista de los jugadores que quedaron fuera de la lista.
   - La lista de los integrantes del comando tecnico.
   - La lista de los estadios donde Peru jugo el mundial.
   - Descripcion o reseña de cualquier estadio del mundial.
   - Descripcion o reseña de cualquiera de los 23 jugadores de futbol.
   - La lista de jugadores peruanos leyendas que disputaron el mundial de futbol.
   - Subscribirse a noticias de la seleccion, validar con SMS.

# Integraciones!

  - Deploy del servicio backend en IBM Cloud e Azure Cloud.
  - Integración con facebook messenger
  - Integración con twitter
  - Integración con Twilio
  - Integración con Watson Assistant

# Consideraciones 
 
 - Se necesita almacenar todas las interacciones del usuario en un mongoDB 